//
//  WonderOfTheWorldObject.h
//  Assignment7V3
//
//  Created by Jason Clinger on 2/27/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@interface WonderOfTheWorldObject : UICollectionViewCell

@property (nonatomic, strong) UIImage* image;
@property (nonatomic, strong) UIImage* image_thumb;
@property (nonatomic, strong) NSString* location;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* region;
@property (nonatomic, strong) NSString* wikipedia;
@property (nonatomic, strong) NSString* year_built;
@property (nonatomic, assign) CLLocationCoordinate2D coord;

@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@property (weak, nonatomic) IBOutlet UIImageView *firstImageView;

@end
