//
//  DetailViewController.h
//  Assignment7V3
//
//  Created by Jason Clinger on 2/27/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController
{
    @public NSString* preName;
    NSString* preBuilt;
    NSString* preLocation;
    NSString* preRegion;
    UIImage* preImage;

}

@property (weak, nonatomic) IBOutlet UILabel *wonderName;
@property (weak, nonatomic) IBOutlet UILabel *wonderBuilt;
@property (weak, nonatomic) IBOutlet UILabel *wonderLocation;
@property (weak, nonatomic) IBOutlet UILabel *wonderRegion;
@property (weak, nonatomic) IBOutlet UIImageView *wonderImage;


@end
