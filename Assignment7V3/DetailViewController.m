//
//  DetailViewController.m
//  Assignment7V3
//
//  Created by Jason Clinger on 2/27/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated{
    _wonderName.text = preName;
    _wonderBuilt.text = preBuilt;
    _wonderLocation.text = preLocation;
    _wonderRegion.text = preRegion;
    _wonderImage.image = preImage;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
