//
//  CollectionViewController.m
//  Assignment7V3
//
//  Created by Jason Clinger on 2/27/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import "CollectionViewController.h"
#import "WonderOfTheWorldObject.h"
#import "DetailViewController.h"

@interface CollectionViewController ()

@end

@implementation CollectionViewController

static NSString * const reuseIdentifier = @"Cell";


typedef void(^myCompletion)(NSArray* data);


- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSData *JSONData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://aasquaredapps.com/Class/sevenwonders.json"]];
    
    NSMutableArray *jsonResult = [NSJSONSerialization JSONObjectWithData:JSONData options:kNilOptions error:nil];
    
    data = JSONData;
    arrayOfSevenWonders = jsonResult;
    
    
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 7;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    WonderOfTheWorldObject *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    d = arrayOfSevenWonders[indexPath.row];    

    cell.name = d[@"name"];
    cell.location = d[@"location"];
    cell.region = d[@"region"];
    cell.wikipedia = d[@"wikipedia"];
    cell.year_built = d[@"year_built"];
    
    //cell.textLabel.text = [NSString stringWithFormat:@"%li", (long)indexPath.row];
    
    cell.textLabel.text = cell.name;
    cell.firstImageView.image = [self imageFromURLString:d[@"image_thumb"]];
    cell.image_thumb = cell.firstImageView.image;
    
    return cell;
}




- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier  isEqual: @"detailview"]) {
        WonderOfTheWorldObject *cell = sender;
        DetailViewController *detailview = segue.destinationViewController;
        
        detailview->preName = cell.name;
        detailview->preBuilt = cell.year_built;
        detailview->preLocation = cell.location;
        detailview->preRegion = cell.region;
        detailview->preImage = cell.image_thumb;
    }
    
}

////////////////////////////////////////////////////////////////////////

- (UIImage *)imageFromURLString:(NSString *)urlString
{
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *result = [NSURLConnection sendSynchronousRequest:request
                                           returningResponse:&response error:&error];
    //[request release];
    //[self handleError:error];
    
    //UIImage *resultImage = [UIImage imageWithData:(NSData *)result];
    
    UIImage* myImage = [UIImage imageWithData:
                        [NSData dataWithContentsOfURL:
                         [NSURL URLWithString: urlString]]];
    
    //[cell.firstImageView setImage: myImage];
    //[myImage release];

    NSLog(@"urlString: %@",urlString);
    return myImage;
}


@end
